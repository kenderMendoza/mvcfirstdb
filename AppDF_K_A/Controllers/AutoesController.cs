﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AppDF_K_A.Models.DAL;

namespace AppDF_K_A.Controllers
{
    public class AutoesController : Controller
    {
        private BD_AutosModel db = new BD_AutosModel();

        // GET: Autoes
        public ActionResult Index()
        {
            var auto = db.Auto.Include(a => a.Detalle_Auto);
            return View(auto.ToList());
        }

        // GET: Autoes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Auto auto = db.Auto.Find(id);
            if (auto == null)
            {
                return HttpNotFound();
            }
            return View(auto);
        }

        // GET: Autoes/Create
        public ActionResult Create()
        {
            ViewBag.Detalle_Auto_IDDetalle_Auto = new SelectList(db.Detalle_Auto, "IDDetalle_Auto", "Tipo_Transmision");
            return View();
        }

        // POST: Autoes/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idAuto,Marca,Modelo,Detalle_Auto_IDDetalle_Auto")] Auto auto)
        {
            if (ModelState.IsValid)
            {
                db.Auto.Add(auto);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Detalle_Auto_IDDetalle_Auto = new SelectList(db.Detalle_Auto, "IDDetalle_Auto", "Tipo_Transmision", auto.Detalle_Auto_IDDetalle_Auto);
            return View(auto);
        }

        // GET: Autoes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Auto auto = db.Auto.Find(id);
            if (auto == null)
            {
                return HttpNotFound();
            }
            ViewBag.Detalle_Auto_IDDetalle_Auto = new SelectList(db.Detalle_Auto, "IDDetalle_Auto", "Tipo_Transmision", auto.Detalle_Auto_IDDetalle_Auto);
            return View(auto);
        }

        // POST: Autoes/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idAuto,Marca,Modelo,Detalle_Auto_IDDetalle_Auto")] Auto auto)
        {
            if (ModelState.IsValid)
            {
                db.Entry(auto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Detalle_Auto_IDDetalle_Auto = new SelectList(db.Detalle_Auto, "IDDetalle_Auto", "Tipo_Transmision", auto.Detalle_Auto_IDDetalle_Auto);
            return View(auto);
        }

        // GET: Autoes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Auto auto = db.Auto.Find(id);
            if (auto == null)
            {
                return HttpNotFound();
            }
            return View(auto);
        }

        // POST: Autoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Auto auto = db.Auto.Find(id);
            db.Auto.Remove(auto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
