﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AppDF_K_A.Models.DAL;

namespace AppDF_K_A.Controllers
{
    public class Detalle_AutoController : Controller
    {
        private BD_AutosModel db = new BD_AutosModel();

        // GET: Detalle_Auto
        public ActionResult Index()
        {
            return View(db.Detalle_Auto.ToList());
        }

        // GET: Detalle_Auto/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Detalle_Auto detalle_Auto = db.Detalle_Auto.Find(id);
            if (detalle_Auto == null)
            {
                return HttpNotFound();
            }
            return View(detalle_Auto);
        }

        // GET: Detalle_Auto/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Detalle_Auto/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Tipo_Transmision,Kilometraje,Tipo_Combustible,Color,IDDetalle_Auto")] Detalle_Auto detalle_Auto)
        {
            if (ModelState.IsValid)
            {
                db.Detalle_Auto.Add(detalle_Auto);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(detalle_Auto);
        }

        // GET: Detalle_Auto/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Detalle_Auto detalle_Auto = db.Detalle_Auto.Find(id);
            if (detalle_Auto == null)
            {
                return HttpNotFound();
            }
            return View(detalle_Auto);
        }

        // POST: Detalle_Auto/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Tipo_Transmision,Kilometraje,Tipo_Combustible,Color,IDDetalle_Auto")] Detalle_Auto detalle_Auto)
        {
            if (ModelState.IsValid)
            {
                db.Entry(detalle_Auto).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(detalle_Auto);
        }

        // GET: Detalle_Auto/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Detalle_Auto detalle_Auto = db.Detalle_Auto.Find(id);
            if (detalle_Auto == null)
            {
                return HttpNotFound();
            }
            return View(detalle_Auto);
        }

        // POST: Detalle_Auto/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Detalle_Auto detalle_Auto = db.Detalle_Auto.Find(id);
            db.Detalle_Auto.Remove(detalle_Auto);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
