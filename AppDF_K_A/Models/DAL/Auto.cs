//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppDF_K_A.Models.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Auto
    {
        public int idAuto { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public int Detalle_Auto_IDDetalle_Auto { get; set; }
    
        public virtual Detalle_Auto Detalle_Auto { get; set; }
    }
}
